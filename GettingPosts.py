import re
import threading
import time

from models.Request import Request
import csv
from random import randint


posts = {}
names = {}
access_token = '5c94af44873fbb6ecfcfd3eac67c737cc32105d6306d7e4431dffce74778251e99af240c8df3c0cccdf9b'
threads = list()


def get_posts(community_id=-108468, quantity=100, offset=0):
    posts = {}
    threads = list()
    req = Request(url='https://api.vk.com/method/execute?', access_token=access_token)
    if quantity%100 > 0:
        raise Exception('Quantity should be multiple 100 and more than 100:' % quantity)
    for num in range(offset, quantity, 100):
        print(num)
        line = 'var r=API.wall.get({"owner_id": '+str(community_id)+', "extended": 1, "filter":"owner ", "fields":"id,text",\
                "count":100, "offset":'+str(num)+'}).items; return [r@.id, r@.text];'
        req.request_params.update({'code': line})
        requsted = req.custom_request()
        data = requsted['response'] if 'response' in requsted else []
        posts.update(parse_data(data))
        names.update(parse_data([data[0], extract_names(data[1])]))
        if num % 100 == 0 and num != 0 and num != quantity-100:
            x = threading.Thread(target=save_posts, args=(None, '.csv', posts.copy(),))
            threads.append(x)
            x.start()
            posts = {}
        elif num == quantity-100:
            save_posts()
        save_posts()


def get_post_parallel(community_id=-108468, quantity=50000, offset=0):
    global threads
    for offs in range(offset, offset + quantity, 100):
        x = threading.Thread(target=get_posts, args=(community_id, 100, offs,))
        threads.append(x)
        x.start()
        if offs%200 == 0:#ubludskie ogranicheniya vk
            print(offs)
            time.sleep(1)


def parse_data(data):
    return dict(zip(data[0], data[1]))


def save_posts(name_file=None, file_format='.csv', posts=None):
    if name_file is None:
        name_file = f"post_data{randint(11, 91575305930)}"
    name_file += file_format

    with open(name_file, 'w', newline='', encoding='utf8') as f:
        writer = csv.DictWriter(f, fieldnames=['post_id', 'text', 'film_name'])
        writer.writeheader()
        for id in posts:
            # if names[id] == '':
            #     continue
            writer.writerow({'post_id': id, 'text': posts[id], 'film_name': names[id]})


def save_posts_parallel(name_file=None, file_format='.csv'):
    for x in threads:
        x.join()
    save_posts(name_file, file_format)


def extract_names(texts):
    regex = r"(\u00AB|«)([-\\/|,.!?а-яА-Я0-9a-zA-Z\s]+)(\u00BB|»)"
    result = []
    for text in texts:
        text = text.replace('\n', '')
        matches = re.split(regex, text, re.MULTILINE)
        name = ''

        try:
            potential_name = matches[2]
            # if not potential_name[0].islower():
            name = potential_name
        except IndexError:
            pass
        result.append(name)
    return result


get_posts(quantity=5000, offset=4000)
