import threading

from models.Likes import  Likes

access_token = '88063e3688063e3688063e36bf8876956c8880688063e36d6851c0983f47b43401f1d54'
Glikes = []

def get_likes(access_token, pid):
    return Likes('-108468', pid).get_likes_list(access_token)

def writeLikes(pid):
    cm = get_likes(access_token, int(pid))
    Glikes.extend(cm)
    # comments = ''
    # if cm is not None:
    #     comments += str(pid)+'\t'+'['+', '.join(str(x) for x in cm)+']'+'\n'
    # else:
    #     return
    # with open('./data/likes' + str(pid) + '.csv', "w", encoding='utf8') as f:
    #     f.writelines(comments)

def read_pids():
    uids = []
    with open('./data/posts_id.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return list(set(uids))


if __name__ == "__main__":
    offset = 0
    pids = read_pids()
    for offset in range(0, 2000, 500):
        threads = list()
        for index in range(500):
            uis = pids[offset]
            x = threading.Thread(target=writeLikes, args=(uis,))
            threads.append(x)
            x.start()
            offset += 1

        for x in threads:
            x.join()
    else:
        threads = list()
        for index in range(344):
            uis = pids[offset]
            x = threading.Thread(target=writeLikes, args=(uis,))
            threads.append(x)
            x.start()
            offset += 1
        for x in threads:
            x.join()
    with open('./data/liked_user_id.csv', "w", encoding='utf8') as f:
        res = '\n'.join(str(x) for x in list(set(Glikes)))
        f.writelines(res)