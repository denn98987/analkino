import threading

from models.User import User
access_token = '88063e3688063e3688063e36bf8876956c8880688063e36d6851c0983f47b43401f1d54'
Gusers = []
def get_users(access_token, uids):
    return User.get_user_by_id(access_token, *uids)

def writePosts(start, uids):
    uss = get_users(access_token, uids)
    users = ''
    for key in uss.keys():
        us = uss[key]
        us = us.__dict__()
        if 'deactivated' in us or 'is_closed' in us:
            users += str(us['id']) + '\t' + us['first_name'] + '\t' + us['last_name'] + '\t'\
            +'\t\t\t[]\n'
        else:
            users += str(us['id'])+'\t' +us['first_name']+'\t' +us['last_name']+'\t'\
            +us['city']+'\t' +str(us['sex'])+'\t' +str(us['bdate'])+'\t'+'['+','.join(us['subscriptions'])+']\n'
    with open('./data/users' + str(start) + '.csv', "w", encoding='utf8') as f:
        f.writelines(users)

def read_uids():
    uids = []
    with open('./data/all_user_ids.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return uids


if __name__ == "__main__":
    offset = 100000
    uids = read_uids()
    # for offset in range(100000, , 50000):
    threads = list()
    for index in range(115):
        if len(uids) > offset + 99:
            x = threading.Thread(target=writePosts, args=(offset,uids[offset:offset + 99],))
        else:
            uis = uids[offset:]
            x = threading.Thread(target=writePosts, args=(offset,uis,))
        threads.append(x)
        x.start()
        offset += 100

    for x in threads:
        x.join()