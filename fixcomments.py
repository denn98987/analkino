import threading

from models.Comment import Comment
from models.Wall import Wall

access_token = '88063e3688063e3688063e36bf8876956c8880688063e36d6851c0983f47b43401f1d54'
Gusers = []

def get_comments(access_token, pid):
    return [Comment(**item) for item in Wall.get_comments(None, '-108468', post_id=pid)]

def writeComments(pid):
    comms = get_comments(access_token, int(pid))
    comments = ''
    for key in comms:
        cm = key
        cm = cm.__dict__()
        if cm is not None:
            comments += str(cm['id'])+'\t'+str(pid)+'\t'+str(cm['user'])+'\t' +cm['text'] +'\n'
        else:
            return
    with open('./data/comments' + str(pid) + '.csv', "w", encoding='utf8') as f:
        f.writelines(comments)

def read_pids():
    uids = []
    with open('./data/posts_id.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return list(set(uids))


if __name__ == "__main__":
    offset = 2000
    pids = read_pids()
    threads = list()
    for index in range(344):
        uis = pids[offset]
        x = threading.Thread(target=writeComments, args=(uis,))
        threads.append(x)
        x.start()
        offset += 1

    for x in threads:
        x.join()