import urllib.request
import urllib.parse
import http.client
import json
from models.RequestException import RequestException


class Request:
    url: str = None
    access_token: str = None
    version = '5.107'

    def __init__(self, url: str,
                 access_token: str = "3a84e6503a84e6503a84e650cc3af44d0a33a843a84e65064642e204890882363217cba",
                 request_params={}):
        self.request_params: {} = {'v': self.version}
        self.url = url
        self.access_token = access_token
        self.request_params.update(request_params)
        self.request_params.update({'access_token': access_token})

    def request(self, extended=0):
        self.request_params.update({'extended': extended})
        query = self._get_full_url()
        result = self._sent_query(query)
        unpacked_data = json.loads(result)

        if 'error' in unpacked_data:
            raise RequestException("Server sent error: " + unpacked_data['error']['error_msg'])
        else:
            if extended == 0:
                return unpacked_data['response']['items']
            else:
                return unpacked_data['response']

    def custom_request(self):
        query = self._get_full_url()
        result = self._sent_query(query)
        unpacked_data = json.loads(result)

        if 'error' in unpacked_data:
            raise RequestException("Server sent error: " + unpacked_data['error']['error_msg'])
        else:
            return unpacked_data


    def _get_full_url(self):
        return self.url + urllib.parse.urlencode(self.request_params)

    @staticmethod
    def _sent_query(query):
        queried = urllib.request.urlopen(query)
        result = http.client.HTTPResponse.read(queried)
        return result
