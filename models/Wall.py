from models.Base import Base
from models.Comment import Comment
from models.Request import Request
import re


class Wall:
    url = 'https://api.vk.com/method/wall.'
    group_id = None
    count = None

    def __init__(self, group_id, count, offset = 0):
        self.group_id = group_id
        self.count = count
        self.offset = offset
        self.posts = []

    def get_wall_posts(self, access_token: str):
        """
        Sent request to vk and return list with wall posts from community id = group_id
        :param access_token: Server access token for authorise request
        :return: list of Post obj
        """
        url = self.url + "get?"
        request_params = {'owner_id': self.group_id, 'count': self.count, 'offset': self.offset}
        conn = Request(url, access_token, request_params=request_params)
        data = conn.request()
        posts = Wall._parse_to_post(data)
        self.posts += posts
        return posts

    @staticmethod
    def _parse_to_post(unparsed_data):
        """
        Change item from request to Post obj
        :param unparsed_data: items from response
        :return: list of Post objs
        """
        return [Post(**item) for item in unparsed_data]

    @staticmethod
    def get_comments(access_token, owner_id, post_id, count=100):
        """
        Sent request to vk to get comments of post
        :param access_token: access token of authorise
        :param owner_id: id of community
        :param post_id: id of post published in that community
        :return: array of items
        """
        url = Wall.url + "getComments?"
        request_params = {'owner_id': owner_id, 'post_id': post_id, 'count': count}
        conn = Request(url=url, request_params=request_params)
        return conn.request()


class Post(Base):
    regex = r"(\u00AB|«)([-\\/|,.!?а-яА-Я0-9a-zA-Z\s]+)(\u00BB|»)"

    def __init__(self, **kwargs):
        kwargs['text'] = kwargs['text'].replace('\n', '')
        matches = re.split(self.regex, kwargs['text'], re.MULTILINE)
        name = None

        try:
            hlname = matches[2]
            if not hlname[0].islower():
                name = hlname
        except IndexError:
            with open('./newdata/logpost.log', "w", encoding='utf8') as f:
                f.writelines(kwargs['text'])
            pass


        self.comments = [Comment(**item) for item in Wall.get_comments(None, kwargs['owner_id'], post_id=kwargs['id'])]
        super().__init__(id=kwargs['id'], text=kwargs['text'], owner_id=kwargs['owner_id'], name=name, comments=self.comments)

    def __dict__(self):
        text = self['text'].replace('\n', ' ')
        return {
            'id': self['id'],
            'name': self['name'],
            'text': text,
            # 'comments': [x.__dict__() for x in self['comments'] if x['text'] != ""]
        }

    def get_comments(self, access_token):
        """
        Take all comments current post from vk
        :param access_token: access token for authorise
        :return: list of Comment obj
        """

        return self['comments']
