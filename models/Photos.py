from models.Base import Base
from models.Request import Request
from models.RequestException import RequestException

class Photos(Base):
    url = 'https://api.vk.com/method/photos.get?'

    @staticmethod
    def get_by_id(access_token, id):
        params = {'owner_id': id, 'album_id': 'wall', 'rev': 1, 'count': 5}
        try:
            requested_wall = Request(Photos.url, access_token, params).request()
        except RequestException:
            requested_wall = []
        params['album_id'] = 'profile'
        try:
            requested_profile = Request(Photos.url, access_token, params).request()
        except RequestException:
            requested_profile = []
        return requested_profile + requested_wall
