from models.Request import Request


class Likes:
    url = 'https://api.vk.com/method/likes.getList?'
    type = None
    owner_id = None
    item_id = None
    count = 1000

    def __init__(self, owner_id, item_id, count=1000, type='post'):
        self.type = type
        self.owner_id = owner_id
        self.item_id = item_id
        self.count = count

    def _get_params(self):
        return {'type': self.type, 'owner_id': self.owner_id, 'item_id': self.item_id, 'count': self.count}

    def get_likes_list(self, access_token):
        request_params = self._get_params()
        return Request(self.url, access_token, request_params).request()
