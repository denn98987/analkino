from models.Base import Base


class Comment(Base):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @property
    def user(self):
        us = None
        # try:
        #     us = self.__getitem__('user')
        # except KeyError:
        us = self.__getitem__('from_id')
        return us

    @user.setter
    def user(self, value):
        self.__setitem__('user', value)

    @property
    def user_id(self):
        return self.__getitem__('from_id')

    def __dict__(self):
        if self['text'] != '':
            text = self['text'].replace('\n', ' ')
            user = self.user
                # if isinstance(self.user, int) else self.user.__dict__()
            return {
                'id': self['id'],
                'text': text,
                'user': user
            }
