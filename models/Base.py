class Base:
    parameters = None

    def __init__(self, **kwargs):
        self.parameters = kwargs

    def __setitem__(self, key, value):
        self.parameters[key] = value

    def __getitem__(self, item):
        return self.parameters[item]

    @property
    def id(self):
        return self['id']

    @id.setter
    def id(self, value):
        self['id'] = value