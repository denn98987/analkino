import threading

from models.Wall import Wall
from models.User import User


class Main:
    access_token = '6649d423dfb70863185e439cea21000e2148b47ecab6ee466094cf94e65c7edec95680b10b3534c3cde95'
    users = []

    def load_data(self, offset=1):
        id_community = -108468
        count_posts = 100
        wall = Wall(id_community, count_posts, offset)
        wall.get_wall_posts(self.access_token)
        posts = [post for post in wall.posts if post['name'] is not None]
        wall.posts = posts
        comments = self.get_comments_(self.access_token, posts)
        self.get_users(self.access_token, comments)
        res = [post.__dict__() for post in posts]
        Main.writePosts(str(offset), res)
        return

    @staticmethod
    def writePosts(name, data):
        posts = ''
        comments = ''
        users = ''
        photos = ''
        for post in data:
            pcomm = ','.join([str(comm['id']) for comm in post['comments'] if len(post['comments'])>0])
            posts += post['name']+', ' +post['text']+', ' +\
                     '"'+pcomm+'" \n'
            for comm in post['comments']:
                us = comm['user']
                if isinstance(us, dict):
                    comments += str(comm['id']) + ', ' + comm['text'] + ', ' + str(comm['user']['id']) + ' \n'
                    if us['id'] not in Main.users:
                        Main.users.append(us['id'])
                        users += str(us['id'])+', ' +us['first_name']+', ' +us['last_name']+', '\
                        +us['city']+', ' +str(us['sex'])+', ' +str(us['bdate'])+', '+'"'+','.join(us['subscriptions'])+'" \n'
                        photos += str(us['id'])+', '+'"'+','.join(us['photos'])+'" \n'
                else:
                    comments += str(comm['id']) + ', ' + comm['text'] + ', ' + str(comm['user']) + ' \n'
                    idu = us
                    users += str(idu)+', ' ", , , , , , \n"
        with open('./newdata/post'+name+".csv", "w", encoding='utf8') as f:
            f.writelines(posts)
        with open('./newdata/comments'+name+'.csv', "w", encoding='utf8') as f:
            f.writelines(comments)
        with open('./newdata/users' + name + '.csv', "w", encoding='utf8') as f:
            f.writelines(users)
        with open('./newdata/photos'+name+'.csv', "w", encoding='utf8') as f:
            f.writelines(photos)

    @staticmethod
    def get_users(access_token, comments):
        uids = []
        for comment in comments:
            if comment.user_id not in uids:
                uids.append(comment.user_id)
        users = {}

        if len(uids) > 1000:
            users = User.get_user_by_id(access_token, *uids[:1000])
            users.update(User.get_user_by_id(access_token, *uids[1000:2000]))
        else:
            users = User.get_user_by_id(access_token, *uids)
        try:
            for comment in comments:
                comment.user = users[comment.user_id]
        except KeyError:
            pass
        return users

    @staticmethod
    def separate_neg_n_pos(comments):
        # sent = SentimentAnalysis()
        neg_comments = []
        pos_comments = []
        # for comment in comments:
        #     if sent.classify(comments) == 0:
        #         neg_comments.append(comment)
        #     else:
        #         pos_comments.append(comment)
        return neg_comments, pos_comments

    @staticmethod
    def get_comments_(access_token, posts):
        comments = []
        for post in posts:
            comments += post.get_groups(access_token)
        return comments


if __name__ == '__main__':
    threads = list()
    m = Main()
    offset = 4000
    for index in range(500):
        x = threading.Thread(target=m.load_data, args=(offset,))
        threads.append(x)
        x.start()
        offset += 10

    for x in threads:
        x.join()

    print("end")
