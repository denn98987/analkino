from models.Base import Base
from models.Request import Request
from models.Photos import Photos
from models.RequestException import RequestException

class User(Base):
    url = 'https://api.vk.com/method/'
    fields = ['city', 'sex', 'bdate']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __dict__(self):
        city = ''
        bdate = ''
        try:
            city = self['city']['title']
            bdate = self['bdate']
        except KeyError:
            pass

        return {
            'id': self['id'],
            'first_name': self['first_name'],
            'last_name': self['last_name'],
            'city': city,
            'sex': self['sex'],
            'bdate': bdate,
            'subscriptions': self['subscriptions']
            # 'photos': [x['photo_130'] for x in self['photos']]
        }

    def request_subscriptions(self, access_token):
        params = {"user_id": self.id, "count": 100}
        url = User.url + "users.getSubscriptions?"
        requested = None
        try:
            requested = Request(url, access_token, params).request(1)['items']
        except RequestException:
            self.__setitem__("subscriptions", [])
            return
        res = []
        for subc in requested:
            if requested is not None and (subc is not None) and ('type' in subc) and (subc['type'] == 'page'):
                res.append(subc['name'])
        self.__setitem__("subscriptions", res)

    def request_photos(self, access_token):
        photos = Photos.get_by_id(access_token, self.id)
        self.__setitem__('photos', photos)

    def request_music(self):
        pass

    @staticmethod
    def __get_extended_user(access_token, **kwargs):
        user = User(**kwargs)
        user.request_subscriptions(access_token)
        # user.request_photos(access_token)
        return user

    @staticmethod
    def __merge_with_comma(arr):
        return ",".join([str(x) for x in arr])

    @staticmethod
    def get_user_by_id(access_token, *ids):
        url = User.url + "users.get?"
        params = {"user_ids": User.__merge_with_comma(ids), "fields": User.__merge_with_comma(User.fields)}
        return {u['id']: User.__get_extended_user(access_token, **u) for u in
                Request(url, access_token, params).request(extended=1)}
