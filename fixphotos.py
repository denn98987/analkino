import threading

from models.Photos import Photos

access_token = '88063e3688063e3688063e36bf8876956c8880688063e36d6851c0983f47b43401f1d54'
Gusers = []
def get_photos(access_token, id):
    return [x['photo_130'] for x in Photos.get_by_id(access_token, id)]

def writePhotos(uid):
    us = get_photos(access_token, uid)
    users = ''
    users += str(uid)+'\t' +'['+','.join(us)+']\n'
    with open('./data/photos' + str(uid) + '.csv', "w", encoding='utf8') as f:
        f.writelines(users)

def read_uids():
    uids = []
    with open('./data/all_user_ids.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return uids


if __name__ == "__main__":
    offset = 0
    pids = read_uids()
    for offset in range(0, 114200, 500):
        threads = list()
        for index in range(500):
            uis = pids[offset]
            x = threading.Thread(target=writePhotos, args=(uis,))
            threads.append(x)
            x.start()
            offset += 1

        for x in threads:
            x.join()
    else:
        threads = list()
        for index in range(206):
            uis = pids[offset]
            x = threading.Thread(target=writePhotos, args=(uis,))
            threads.append(x)
            x.start()
            offset += 1
        for x in threads:
            x.join()