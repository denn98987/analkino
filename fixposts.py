import threading

from models.Wall import Wall


class FixPosts:
    access_token = '88063e3688063e3688063e36bf8876956c8880688063e36d6851c0983f47b43401f1d54'
    users = []

    def load_data(self, offset=1):
        id_community = -108468
        count_posts = 9
        wall = Wall(id_community, count_posts, offset)
        wall.get_wall_posts(self.access_token)
        posts = [post for post in wall.posts if post['name'] is not None]
        res = [post.__dict__() for post in posts]
        FixPosts.writePosts(str(offset), res)

    @staticmethod
    def writePosts(name, data):
        posts = ''
        for post in data:
            posts += str(post['id'])+'\t'+post['name']+'\t' +post['text']+'"\n'
        with open('./data/posts'+name+".csv", "w", encoding='utf8') as f:
            f.writelines(posts)


if __name__ == '__main__':
    threads = list()
    m = FixPosts()
    offset = 5000
    for index in range(500):
        x = threading.Thread(target=m.load_data, args=(offset,))
        threads.append(x)
        x.start()
        offset += 10

    for x in threads:
        x.join()

    print("end")
