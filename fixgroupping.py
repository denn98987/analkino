from models.Request import Request
from models.RequestException import RequestException

access_token = '88063e3688063e3688063e36bf8876956c8880688063e36d6851c0983f47b43401f1d54'
Glikes = []

def get_groups(pid):
    code = 'return   API.groups.get({"user_id": '+str(pid)+', "extended": 1, "filter":"publics", ' \
                                                      '"fields":"activity"}).items@.activity; '
    req = Request(url='https://api.vk.com/method/execute?', request_params={'code': code}).custom_request()
    return req

def writeGroups(pid):
    grs = []
    req = None
    try:
        req = get_groups(pid)
    except RequestException:
        pass
    if req is not None and 'response' in req:
        grs = req['response']
    grs = [str(x) for x in grs]
    activities = str(pid) + '\t' + ','.join(grs)+'\n'

    with open('./data/gr_ac' + str(pid) + '.csv', "w", encoding='utf8') as f:
        f.writelines(activities)

def read_pids():
    uids = []
    with open('./data/f/lastuids.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return list(set(uids))


if __name__ == "__main__":
    offset = 0
    pids = read_pids()
    # writeGroups(100291578)
    # for m in range(0, 1000, 500):
    #     for offset in range(500):
    #         threads = list()
    #         for index in range(500):
    #             uis = pids[offset+m]
    #             x = threading.Thread(target=writeGroups, args=(uis,))
    #             threads.append(x)
    #             x.start()
    #             offset += 1
    #         #
    #         for x in threads:
    #             x.join()
    # else:
    #     threads = list()
    #     for index in range(1000, 1317):
    #         uis = pids[offset]
    #         x = threading.Thread(target=writeGroups, args=(uis,))
    #         threads.append(x)
    #         x.start()
    #         offset += 1
        # for x in threads:
        #     x.join()
    for off in range(342):
        writeGroups(pids[off])