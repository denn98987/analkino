import threading
import re
from multiprocessing import Pool

import pandas as pd
import pymorphy2
import swifter
from urllib.parse import quote
import requests
from bs4 import BeautifulSoup


def del_errors(name_file='post_data.csv'):
    with open(name_file, 'r', errors='ignore', encoding='utf-8') as r:
        f = r.read()
    with open(name_file, 'w', errors='ignore', encoding='utf-8') as r:
        r.write(f)


def morph_base(df):
    morph = pymorphy2.MorphAnalyzer()
    film_name = list(df)[2]
    df[film_name] = df[film_name].apply(lambda s: re.sub(r'[^а-яА-Я ]', ' ', str(s)))
    df[film_name] = df[film_name].apply(lambda s: re.sub(' +', ' ', str(s)))
    df['normal_name'] = df[film_name].apply(
        lambda x: ' '.join([morph.parse(y)[0].normal_form for y in str(x).split()]))
    return df


def morphizate(df):
    return df.apply(morph_base)


# def request(name):
#     param = quote(name.encode('cp1251'))
#     url = 'https://www.kinopoisk.ru/index.php?kp_query='+param
#     headers = {
#             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0'
#           }
#     r = requests.get(url, headers=headers)
#     text = r.text
#     soup = BeautifulSoup(text)
#     mostwanted = None
#     hl = None
#     try:
#         sel = soup.select('.most_wanted > div:nth-child(3) > p:nth-child(1) > a:nth-child(1)')
#         mostwanted = sel[0].text
#         link0 = sel[0]['data-url']
#     except IndexError:
#         pass
#     try:
#         sel = soup.select('div.search_results:nth-child(4) > div:nth-child(2) > div:nth-child(3) > p:nth-child(1) > a:nth-child(1)')
#         hl = sel[0].text
#         link1 = sel[0]['data-url']
#     except IndexError:
#         pass
#     return (mostwanted, hl)
#
#
# def writeNames(name, id):
#     res = request(name)
#     nnames = name+'\t'
#     if res[0] is not None:
#         nnames += res[0]+'\t'+res[1]+'\t'
#     if res[2] is not None:
#         nnames += res[2] + '\t' + res[3] + '\n'
#     else:
#         return
#     with open('./data/name' + str(id) + '.csv', "w", encoding='utf8') as f:
#         f.writelines(nnames)


def post_handle(name_file='post_data.csv'):
    del_errors(name_file)
    df = pd.read_csv(name_file, encoding='utf-8')
    morphizate(df)
    # dfs = []
    # for i in range(5):
    #     if i == 4:
    #         dfs.append(df[len(df) // 5 * i:])#for size%5!=0
    #         break
    #     dfs.append(df[len(df)//5*i:len(df)//5*(i+1)])
    # p = Pool()
    # result_list = p.map(morphizate, dfs)
    pass


if __name__ == '__main__':
    post_handle()