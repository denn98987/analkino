from urllib.parse import quote
import requests
from bs4 import BeautifulSoup

def request(name):
    param = quote(name.encode('cp1251'))
    url = 'https://www.kinopoisk.ru/index.php?kp_query='+param
    headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0'
          }
    r = requests.get(url, headers=headers)
    text = r.text
    soup = BeautifulSoup(text)
    mostwanted = None
    link0 = None
    link1 = None
    hl = None
    try:
        sel = soup.select('.most_wanted > div:nth-child(3) > p:nth-child(1) > a:nth-child(1)')
        mostwanted = sel[0].text
        link0 = sel[0]['data-url']
    except IndexError:
        pass
    try:
        sel = soup.select('div.search_results:nth-child(4) > div:nth-child(2) > div:nth-child(3) > p:nth-child(1) > a:nth-child(1)')
        hl = sel[0].text
        link1 = sel[0]['data-url']
    except IndexError:
        pass
    return (mostwanted, link0, hl, link1)

def read_names():
    uids = []
    with open('./data/f/names.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return list(set(uids))

def writeNames(name, id):
    res = request(name)
    nnames = name+'\t'
    if res[0] is not None:
        nnames += res[0]+'\t'+res[1]+'\t'
    if res[2] is not None:
        nnames += res[2] + '\t' + res[3] + '\n'
    else:
        return
    with open('./data/name' + str(id) + '.csv', "w", encoding='utf8') as f:
        f.writelines(nnames)

if __name__ == "__main__":
    offset = 0
    pids =read_names()
    for x in range(886):
        film = pids[x]
        writeNames(film, x)
    # for offset in range(500):
    #     threads = list()
    #     for index in range(500):
    #         uis = pids[offset]
    #         x = threading.Thread(target=writeNames, args=(uis,))
    #         threads.append(x)
    #         x.start()
    #         offset += 1
    #
    #     for x in threads:
    #         x.join()
    # else:
    #     threads = list()
    #     for index in range(386):
    #         uis = pids[offset]
    #         x = threading.Thread(target=writeNames, args=(uis,))
    #         threads.append(x)
    #         x.start()
    #         offset += 1
    #     for x in threads:
    #         x.join()