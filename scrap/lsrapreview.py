import threading
from urllib.parse import quote
from lxml import html
import glob

import requests
from bs4 import BeautifulSoup
import logging
marks = {
    'b': '0',
    'g': '1',
    'n': '2'
}

def parse(text):
    soup = BeautifulSoup(text, 'html.parser')
    sel = soup.select('span._reachbanner_')
    return [x.text for x in sel]

def read_files(name):
    files = glob.glob("./data/*.html")
    for fname in files:
        with open(fname, "r", encoding='utf8') as f:
            yield f.read()

def write(doc, name):
    res = parse(doc)

    with open(f'datareviews2.csv', "a", encoding='utf8') as f:
        logging.info(f'datareviews.csv start writes')
        for rev in res:
            text = rev.replace('\n\r\n', '').replace('\n', '').replace('\r', '').replace('\t', '')
            f.write(marks[name.split('\\')[1][0]]+'\t'+text+'\n')

def writeReviews():
    files = glob.glob("./data/*.html")
    threads = list()
    for fname in files:
        with open(fname, "r", encoding='utf8', errors='ignore') as f:
            x = threading.Thread(target=write, args=(f.read(),fname,))
            threads.append(x)
    for x in range(500):
        threads[x].start()
    for x in range(500):
        threads[x].join()
    for x in range(500, 978):
        threads[x].start()

if __name__ == "__main__":
    logging.basicConfig(filename="reviews.log", level=logging.INFO)
    writeReviews()