import threading
from urllib.parse import quote
from lxml import html

import requests
from bs4 import BeautifulSoup
import logging
marks = {
    'bad': '0',
    'good': '1',
    'neutral': '2'
}

def request(link, mark):
    url = f'https://www.kinopoisk.ru{link}/reviews/?status={mark}&ord=rating'

    headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0'
          }
    logging.info(f'web request to{link}')
    r = requests.get(url, headers=headers)
    text = r.text
    soup = BeautifulSoup(text, 'html.parser')
    result = []
    try:
        sel = soup.select('span._reachbanner_')
        #soup.findAll('a', class_='_reachbanner_')
        result = [x.text for x in sel]
    except IndexError:
        logging.error(url+':'+str(r.status_code))
        with open('./data/err/errors.txt', "a", encoding='utf8') as f:
            f.write(url+'\n')
    return result

def read_names():
    uids = []
    with open('../data/f/filmlinks.csv', "r", encoding='utf8') as f:
        uids = [line.rstrip() for line in f]
    return uids

def write(link, mark, rid):
    res = request(link, mark)
    if len(res) == 0:
        logging.info(f'{link} null. Nas obnaruzhili')
        return
    with open('./data/review' + str(rid) +mark+ '.csv', "a", encoding='utf8') as f:
        logging.info(f'{rid} start writes')
        for rev in res:
            text = rev.replace('\n\r\n','').replace('\n', '').replace('\r', '').replace('\t', '')
            f.write(marks[mark]+'\t'+text+'\n')

def writeReviews(name, id):
    logging.info(f'start {name} mark: good')
    thrg = threading.Thread(target=write, args=(name, 'good', id,))
    thrg.start()
    logging.info(f'start {name} mark: bad')
    thrb = threading.Thread(target=write, args=(name, 'bad', id,))
    thrb.start()
    logging.info(f'start {name} mark: neutral')
    thrn = threading.Thread(target=write, args=(name, 'neutral', id,))
    thrn.start()


if __name__ == "__main__":
    logging.basicConfig(filename="reviews.log", level=logging.INFO)
    offset = 0
    pids =read_names()
    threads = list()
    for offset in range(1, 300):
        uis = pids[offset]
        x = threading.Thread(target=writeReviews, args=(uis,offset,))
        threads.append(x)
        x.start()

    # else:
    #     threads = list()
    #     for index in range(386):
    #         uis = pids[offset]
    #         x = threading.Thread(target=writeNames, args=(uis,))
    #         threads.append(x)
    #         x.start()
    #         offset += 1
    #     for x in threads:
    #         x.join()